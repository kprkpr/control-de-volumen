package com.softwaretalent.controldevolumen;

import android.content.Context;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;


public class MainActivity extends AppCompatActivity {
    private SeekBar sys_seekV = null;
    private SeekBar music_seekV = null;
    private SeekBar notif_seekV = null;
    private SeekBar alarm_seekV = null;
    private AudioManager audioManager = null;
    public int as = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setVolumeControlStream(AudioManager.STREAM_NOTIFICATION);
        initControls();
        Button btnSilence = (Button) findViewById(R.id.btn_silence);
        btnSilence.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (as == 1) {
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                    as = 2;
                }else if (as == 2)
                {
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    as = 1;
                }
            }
        });
    }

    private void initControls() {
        try {
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

            sys_seekV = (SeekBar) findViewById(R.id.sys_seek);
            sys_seekV.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM));
            sys_seekV.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM));

            sys_seekV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM,
                            progress, 0);
                }
            });

            music_seekV = (SeekBar) findViewById(R.id.music_seek);
            music_seekV.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            music_seekV.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

            music_seekV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });

            notif_seekV = (SeekBar) findViewById(R.id.notif_seek);
            notif_seekV.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION));
            notif_seekV.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));

            notif_seekV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                            progress, 0);
                }
            });
            alarm_seekV = (SeekBar) findViewById(R.id.alarm_seek);
            alarm_seekV.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM));
            alarm_seekV.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_ALARM));

            alarm_seekV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_ALARM,
                            progress, 0);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
